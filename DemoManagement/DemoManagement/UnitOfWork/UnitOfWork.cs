﻿using System;
using DemoManagement.Models;
using DemoManagement.Reponsitory.GenericReponsitory;

namespace DemoManagement.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private DemoDBContext _context;

        public UnitOfWork(DemoDBContext context)
        {
            _context = context;
            InitReponsitory();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IGenericReponsitory<User> UserReponsitory { get; private set; }
        public IGenericReponsitory<Request> RequestReponsitory { get; private set; }
        
        private void InitReponsitory()
        {
            UserReponsitory = new GenericReponsitory<User>(_context);
            RequestReponsitory = new GenericReponsitory<Request>(_context);
        }
        public void Save()
        {
            _context.SaveChanges();
        }

    }
}
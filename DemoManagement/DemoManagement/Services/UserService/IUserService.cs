﻿using System.Collections.Generic;
using DemoManagement.Models;

namespace DemoManagement.Services.UserService
{
    public interface IUserService
    {
        User GetById(int? id);
        IEnumerable<User> GetAll();
        void Add(User user);
        void Delete(int id);
        void Update(User user);
        void Save();
        bool Login(string Email, string Password);
        User GetEmail(string Email);
    }
}
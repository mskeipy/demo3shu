﻿using System.Collections.Generic;
using DemoManagement.Models;
using DemoManagement.Reponsitory.RequestReponsitory;
using DemoManagement.UnitOfWork;

namespace DemoManagement.Services.RequestService
{
    public class RequestService : IRequestService
    {
        private IUnitOfWork _unitOfWork;

        public RequestService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Request GetById(int? id)
        {
            return _unitOfWork.RequestReponsitory.GetById(id);
        }

        public IEnumerable<Request> GetAll()
        {
            return _unitOfWork.RequestReponsitory.GetAll();
        }

        public void Add(Request request)
        {
            _unitOfWork.RequestReponsitory.Add(request);
        }

        public void Delete(int id)
        {
            _unitOfWork.RequestReponsitory.Delete(id);
        }

        public void Update(Request request)
        {
            _unitOfWork.RequestReponsitory.Update(request);
        }

        public void Save()
        {
            _unitOfWork.RequestReponsitory.Save();
        }
    }
}
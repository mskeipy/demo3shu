﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace DemoManagement.Models
{
    public class User 
    {
       
        public int UserId { get; set; }
        
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required]
        [StringLength(30, ErrorMessage = "Password must be least 6 character long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string PassWord { get; set; }
        
        [Required]
        public bool IsActive { get; set; }
        
        [Required]
        public int RoleId { get; set; }
        public Role Role { get; set; }
        
    }
}
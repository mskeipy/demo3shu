﻿namespace DemoManagement.Models
{
    public class Request
    {
        public int RequestId { get; set; }
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string Content { get; set; }
    }
}
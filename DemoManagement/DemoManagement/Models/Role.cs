﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DemoManagement.Models
{
    public class Role
    {
        public int RoleId { get; set; }
        
        [Required]
        public string RoleName { get; set; }
        
        [Required]
        public string Note { get; set; }
        
        public ICollection<User> Users { get; set; }
    }
}
﻿using AutoMapper;
using DemoManagement.Models;

namespace DemoManagement.ViewModels
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<User, UserViewModels>();
            CreateMap<User, LoginViewModel>();
        }
    }
}
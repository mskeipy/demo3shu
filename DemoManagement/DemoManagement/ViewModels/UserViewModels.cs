﻿namespace DemoManagement.ViewModels
{
    public class UserViewModels
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Phone { get; set; }
        public string PassWord { get; set; }
        public int Role { get; set; }
        public bool IsActive { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace DemoManagement.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string PassWord { get; set; }
    }
}
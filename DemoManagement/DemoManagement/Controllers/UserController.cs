﻿using DemoManagement.Models;
using DemoManagement.Services.UserService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DemoManagement.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[Controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        // GET
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Index()
        {
            return
            View(_userService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            _userService.Add(user);
            _userService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var getId = _userService.GetById(id);
            return View(getId);
        }

        [HttpPut]
        public IActionResult Edit(User user)
        {
            _userService.Update(user);
            _userService.Save();
            return RedirectToAction("Index");
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            _userService.Save();
            return RedirectToAction("Index");
        }
    }
}
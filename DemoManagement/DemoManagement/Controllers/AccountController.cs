﻿using System.Collections.Generic;
using System.Security.Claims;
using DemoManagement.Models;
using DemoManagement.Services.UserService;
using DemoManagement.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace DemoManagement.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }
        
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(User userModel)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.Login(userModel.Email, userModel.PassWord);

                if (user)
                {
                    var email = _userService.GetEmail(userModel.Email);
                    var role = "";
                    if (email.RoleId == 1)
                    {
                        role = "Admin";
                    }
                    else
                    {
                        role = "User";
                    }
                }

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Email, userModel.Email),
                    new Claim()
                };
            }
        }
    }
}
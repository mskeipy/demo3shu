﻿using DemoManagement.Models;
using DemoManagement.Services.RequestService;
using Microsoft.AspNetCore.Mvc;

namespace DemoManagement.Controllers
{
    public class RequestController : Controller
    {
        private readonly IRequestService _requestService;

        public RequestController(IRequestService requestService)
        {
            _requestService = requestService;
        }
        // GET
        [HttpGet]
        public IActionResult Index()
        {
            return
            View(_requestService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Request request)
        {
            _requestService.Add(request);
            _requestService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var getId = _requestService.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Edit(Request request)
        {
            _requestService.Update(request);
            _requestService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _requestService.Delete(id);
            _requestService.Save();
            return RedirectToAction("Index");
        }
    }
}
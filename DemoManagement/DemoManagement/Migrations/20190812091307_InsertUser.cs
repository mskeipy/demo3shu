﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DemoManagement.Migrations
{
    public partial class InsertUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "FirstName", "IsActive", "LastName", "PassWord", "Phone", "Role" },
                values: new object[] { 1, "nguyena123@gmail.com", "Nguyen", true, "A", "123456", 123456789, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1);
        }
    }
}
